﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace proximityeval.salesorder.api.common
{
    public class DatabaseConfiguration: ConfigurationBase
    {
        private string DbConnectionKey = "proximityevalSalesOrderApiConnection";
        public string GetDatabaseConnectionString()
        {
            return GetConfiguration().GetConnectionString(DbConnectionKey);
        }
    }
}
