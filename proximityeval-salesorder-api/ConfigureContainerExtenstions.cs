﻿using System.Collections.Generic;
using System.IO;
using proximityeval.salesorder.api.common;
using proximityeval.salesorder.api.dal;
using proximityeval.salesorder.api.dal.Interfaces;
using AutoMapper;
using proximityeval_salesorder_api.Helpers;
using proximityeval.salesorder.api.persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyModel;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace proximityeval_salesorder_api
{
    public static class ConfigureContainerExtenstions
    {
        private static string DbConnectionString => new DatabaseConfiguration().GetDatabaseConnectionString();
        private static string CorsPolicyName => new CorsConfiguration().GetCorsPolicyName();

        public static void AddDbContext(this IServiceCollection serviceCollection,
            string connectionString = null)
        {
            serviceCollection.AddDbContext<ProximityEvalContext>(options =>
                // DB dialect
                // options.UseSqlite(connectionString ?? DbConnectionString));
                options.UseSqlServer(connectionString ?? DbConnectionString));
        }

        public static void AddTransientServices(this IServiceCollection serviceCollection)
        {

            // Authentication and roles
            serviceCollection.AddTransient<IRoleService, RoleService>();
            serviceCollection.AddTransient<IUserService, UserService>();

            // Eval services
            serviceCollection.AddTransient<ICustomerService, CustomerService>();
            serviceCollection.AddTransient<IProductService, ProductService>();
            serviceCollection.AddTransient<IOrderService, OrderService>();
            serviceCollection.AddTransient<IOrderDetailService, OrderDetailService>();
           

        }

        public static void AddCustomizedMvc(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddMvc();
        }

        
        public static void AddCorsPolicy(this IServiceCollection serviceCollection, string corsPolicyName = null)
        {
            serviceCollection.AddCors(options =>
            {
                options.AddPolicy(corsPolicyName ?? CorsPolicyName,
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
        }



       
        public static void AddSwagger(this IServiceCollection serviceCollection, string versionNumberString,
            bool includeXmlDocumentation = true)
        {
            serviceCollection.AddSwaggerGen(options =>
            {
                // Specify two versions 
                options.SwaggerDoc("v1",
                    new Info()
                    {
                        Version = "v1",
                        Title = "Proximity Software - Sales Order API - v1",
                        Description = "Proximity Software - Sales Order API - v1",
                        TermsOfService = "Proximity Software - Sales Order API - v1",
                        Contact = new Contact
                        {
                            Name = "R@ul Berrios Rivera",
                            Email = "raul.berrios@outlook.com",
                            Url = ""
                        }
                    }); ;

                // This call remove version from parameter, without it we will have version as parameter 
                // for all endpoints in swagger UI
                options.OperationFilter<RemoveVersionFromParameter>();

                // This make replacement of v{version:apiVersion} to real version of corresponding swagger doc.
                options.DocumentFilter<ReplaceVersionWithExactValueInPath>();

                
                options.DocInclusionPredicate((version, desc) =>
                {
                    if (!desc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;
                    var versions = methodInfo.DeclaringType
                            .GetCustomAttributes(true)
                            .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);
                    return versions.Any(v => $"v{v.ToString()}" == version);
                });

                var security = new Dictionary<string, IEnumerable<string>>
                    {
                        {"Bearer",  new string[] { }},
                    };

                options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"

                });
                options.AddSecurityRequirement(security);


                if (!includeXmlDocumentation) return;
                // Set the comments path for the Swagger JSON and UI.
                var basePath = Directory.GetCurrentDirectory();
                var xmlPath = Path.Combine(basePath, "proximityevalSalesOrderApi.xml");
                if (File.Exists(xmlPath))
                {
                    options.IncludeXmlComments(xmlPath);
                }

                options.OperationFilter<AuthorizationHeaderParameterOperationFilter>();

            }); // End Swagger gen


        }


        
        public static void AddSwagger(this IServiceCollection serviceCollection, bool includeXmlDocumentation = true)
        {
            
            serviceCollection.AddSwaggerGen(options =>
            {
                // Specify two versions 
                options.SwaggerDoc("v1",
                    new Info()
                    {
                        Version = "v1",
                        Title = "Proximity Software - Sales Order API - v1",
                        Description = "Proximity Software - Sales Order API - v1",
                        TermsOfService = "Evaluation API Test Usage v1",
                        Contact = new Contact
                        {
                            Name = "R@ul Berrios Rivera",
                            Email = "raul.berrios@outlook.com",
                            Url = ""
                        }
                    }); ;

                
                options.OperationFilter<RemoveVersionFromParameter>();

                // This make replacement of v{version:apiVersion} to real version of corresponding swagger doc.
                options.DocumentFilter<ReplaceVersionWithExactValueInPath>();

                options.DocInclusionPredicate((version, desc) =>
                {
                    var versions = desc.ControllerAttributes()
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    var maps = desc.ActionAttributes()
                        .OfType<MapToApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions)
                        .ToArray();

                    return versions.Any(v => $"v{v.ToString()}" == version) && (maps.Length == 0 || maps.Any(v => $"v{v.ToString()}" == version));
                });

                var security = new Dictionary<string, IEnumerable<string>>
                    {
                        {"Bearer",  new string[] { }},
                    };

                options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"

                });
                options.AddSecurityRequirement(security);


                if (!includeXmlDocumentation) return;
                // Set the comments path for the Swagger JSON and UI.
                var basePath = Directory.GetCurrentDirectory();
                var xmlPath = Path.Combine(basePath, "saonGroupJobDashboardApi.xml");
                if (File.Exists(xmlPath))
                {
                    options.IncludeXmlComments(xmlPath);
                }

            }); // End Swagger gen
        }
    }
}
