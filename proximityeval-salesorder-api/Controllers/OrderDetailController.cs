﻿using Microsoft.AspNetCore.Mvc;
using proximityeval.salesorder.api.dto.MapperObjects;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.dal.Interfaces;
using proximityeval.salesorder.api.dto.Helpers;
using AutoMapper;
using System.Collections.Generic;
using proximityeval_salesorder_api.Controllers;
using proximityeval_salesorder_api.Helpers;

namespace proximityeval_salesorderDetail_api.Controllers
{
    //[Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [Produces("application/json")]
    [ApiController]
    public class OrderDetailController : BaseController
    {
        private readonly IOrderDetailService _orderDetailService;
        private IMapper _mapper;


        public OrderDetailController(IOrderDetailService orderDetailService,
            IMapper mapper)
        {
            _orderDetailService = orderDetailService;
            _mapper = mapper;
        }


        [HttpGet("Get/{id}")]
        public JsonResult GetByOrdinal(int id)
        {
            var orderDetail = _orderDetailService.FindByOrdinal(id);
            if (orderDetail == null)
            {
                return ErrorResponse("Not found");
            }

            return SingleResult(OrderDetailViewModelHelper.ConvertToViewModel(orderDetail));
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var orderDetails = _orderDetailService.GetAll();
            var orderDetailsDTO = _mapper.Map<IList<OrderDetailDto>>(orderDetails);
            return Ok(orderDetailsDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var orderDetail = _orderDetailService.GetById(id);
            var orderDetailDTO = _mapper.Map<OrderDetailDto>(orderDetail);
            return Ok(orderDetailDTO);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _orderDetailService.Delete(id);
            return Ok();
        }


        [HttpPost("Create")]
        public IActionResult Create([FromBody]OrderDetailDto orderDetailDto)
        {
            var orderDetail = _mapper.Map<OrderDetail>(orderDetailDto);
            try
            {
                // var ex = orderDetailDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                var obj = _orderDetailService.Create(orderDetail,
                                                    orderDetail.Product.Id,
                                                    orderDetail.OrderId,
                                                    orderDetail.Description,
                                                    orderDetail.Quantity,
                                                    orderDetail.UnitPrice,
                                                    orderDetail.Status);
                return Ok(new
                {
                    id = obj.Id, 
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create

        
        [HttpPost("Update")]
        public IActionResult Update([FromBody]OrderDetailDto orderDetailDto)
        {
            var orderDetail = _mapper.Map<OrderDetail>(orderDetailDto);
            try
            {
                // var ex = orderDetailDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                var cust = _orderDetailService.Update(orderDetail,
                                                    orderDetail.Product.Id,
                                                    orderDetail.OrderId,
                                                    orderDetail.Description,
                                                    orderDetail.Quantity,
                                                    orderDetail.UnitPrice,
                                                    orderDetail.Status);
                return Ok(new
                {
                    Id = cust.Id,
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create



    } // End Class
}
