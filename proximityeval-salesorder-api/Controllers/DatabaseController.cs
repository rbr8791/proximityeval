﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using proximityeval.salesorder.api.dto.MapperObjects;
using proximityeval.salesorder.api.dal;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.dal.Interfaces;
using proximityeval.salesorder.api.dto.Helpers;
using AutoMapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Configuration;
using Microsoft.AspNetCore.Authorization;
using proximityeval_salesorder_api.Helpers;
using proximityeval_salesorder_api.Controllers;
using Microsoft.Extensions.Configuration;

namespace proximityeval_salesorder_api.Controllers
{
    // [Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [Produces("application/json")]
    [ApiController]
    public class DatabaseController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly IDatabaseService _databaseService;

        public DatabaseController(IConfiguration configuration, IDatabaseService databaseService)
        {
            _configuration = configuration;
            _databaseService = databaseService;
        }

        /// <summary>
        /// Used to Seed the Database (using JSON files which are included with the application)
        /// </summary>
        /// <returns>
        /// A <see cref="BaseController.MessageResult"/> with the number of entities which
        /// were added to the database.
        /// </returns>
        [HttpGet("SeedData")]
        public JsonResult SeedData()
        {
            var entitiesAdded = _databaseService.SeedDatabase();

            return MessageResult($"Number of new entities added: {entitiesAdded}");

        }

        /// <summary>
        /// Used to drop all current data from the database and recreate any tables
        /// </summary>
        /// <param name="secret">
        /// A passphrase like secret to ensure that a Drop Data action should take place
        /// </param>
        /// <returns>
        /// A <see cref="BaseController.MessageResult"/>
        /// </returns>
        [HttpDelete("DropData")]
        public JsonResult DropData(string secret = null)
        {
            if (SecretChecker.CheckUserSuppliedSecretValue(secret,
                _configuration["dropDatabaseSecretValue"]))
            {
                return ErrorResponse("Incorrect secret");
            }

            var success = _databaseService.ClearDatabase();

            return MessageResult("Database tabled dropped and recreated", success);
        }


    } // End Class
}
