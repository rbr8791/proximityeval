﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using proximityeval.salesorder.api.dto.MapperObjects;
using proximityeval.salesorder.api.dal;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.dal.Interfaces;
using proximityeval.salesorder.api.dto.Helpers;
using AutoMapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Configuration;
using Microsoft.AspNetCore.Authorization;
using proximityeval_salesorder_api.Helpers;
using proximityeval_salesorder_api.Controllers;

namespace proximityeval_salesorder_api.Controllers
{
    //[Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [Produces("application/json")]
    [ApiController]
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;
        private IMapper _mapper;


        public ProductController(IProductService productService,
            IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }


        [HttpGet("Get/{id}")]
        public JsonResult GetByOrdinal(int id)
        {
            var product = _productService.FindByOrdinal(id);
            if (product == null)
            {
                return ErrorResponse("Not found");
            }

            return SingleResult(ProductViewModelHelper.ConvertToViewModel(product));
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var products = _productService.GetAll();
            var productsDTO = _mapper.Map<IList<ProductDto>>(products);
            return Ok(productsDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var product = _productService.GetById(id);
            var productDTO = _mapper.Map<ProductDto>(product);
            return Ok(productDTO);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _productService.Delete(id);
            return Ok();
        }


        [HttpPost("Create")]
        public IActionResult Create([FromBody]ProductDto productDto)
        {
            var product = _mapper.Map<Product>(productDto);
            try
            {
                // var ex = productDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                var obj = _productService.Create(product, productDto.ProductName, 
                    productDto.UnitPrice, productDto.UnitsInStock, productDto.Status);
                return Ok(new
                {
                    id = obj.Id, 
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create

        
        [HttpPost("Update")]
        public IActionResult Update([FromBody]ProductDto productDto)
        {
            var product = _mapper.Map<Product>(productDto);
            try
            {
                // var ex = productDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                var cust = _productService.Update(product, productDto.ProductName,
                    productDto.UnitPrice, productDto.UnitsInStock, productDto.Status);
                return Ok(new
                {
                    Id = cust.Id,
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create



    } // End Class
}
