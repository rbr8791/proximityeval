﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using proximityeval.salesorder.api.dto.MapperObjects;
using proximityeval.salesorder.api.dal;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.dal.Interfaces;
using proximityeval.salesorder.api.dto.Helpers;
using AutoMapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Configuration;
using Microsoft.AspNetCore.Authorization;
using proximityeval_salesorder_api.Helpers;
using proximityeval_salesorder_api.Controllers;

namespace proximityeval_salesorder_api.Controllers
{
    // [Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [Produces("application/json")]
    [ApiController]
    public class CustomerController : BaseController
    {
        private readonly ICustomerService _customerService;
        private IMapper _mapper;


        public CustomerController(ICustomerService customerService,
            IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
        }


        [HttpGet("Get/{id}")]
        public JsonResult GetByOrdinal(int id)
        {
            var customer = _customerService.FindByOrdinal(id);
            if (customer == null)
            {
                return ErrorResponse("Not found");
            }

            return SingleResult(CustomerViewModelHelper.ConvertToViewModel(customer));
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var customers = _customerService.GetAll();
            var customersDTO = _mapper.Map<IList<CustomerDto>>(customers);
            return Ok(customersDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var customer = _customerService.GetById(id);
            var customerDTO = _mapper.Map<CustomerDto>(customer);
            return Ok(customerDTO);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _customerService.Delete(id);
            return Ok();
        }


        [HttpPost("Create")]
        public IActionResult Create([FromBody]CustomerDto customerDto)
        {
            var customer = _mapper.Map<Customer>(customerDto);
            try
            {
                // var ex = customerDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                var cust = _customerService.Create(customer, customerDto.CustomerName, 
                    customerDto.Address, customerDto.City, customerDto.Region, customerDto.PostalCode,
                    customerDto.Phone, customerDto.Status);
                return Ok(new
                {
                    id = cust.Id, 
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create

        
        [HttpPost("Update")]
        public IActionResult Update([FromBody]CustomerDto customerDto)
        {
            var customer = _mapper.Map<Customer>(customerDto);
            try
            {
                // var ex = customerDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                var cust = _customerService.Update(customer, customerDto.CustomerName,
                    customerDto.Address, customerDto.City, customerDto.Region, customerDto.PostalCode,
                    customerDto.Phone, customerDto.Status);
                return Ok(new
                {
                    Id = cust.Id,
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create



    } // End Class
}
