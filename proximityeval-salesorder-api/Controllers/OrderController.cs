﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using proximityeval.salesorder.api.dto.MapperObjects;
using proximityeval.salesorder.api.dal;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.dal.Interfaces;
using proximityeval.salesorder.api.dto.Helpers;
using AutoMapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Configuration;
using Microsoft.AspNetCore.Authorization;
using proximityeval_salesorder_api.Helpers;
using proximityeval_salesorder_api.Controllers;


namespace proximityeval_salesorder_api.Controllers
{
    //[Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [Produces("application/json")]
    [ApiController]
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;
        private IMapper _mapper;


        public OrderController(IOrderService orderService,
            IMapper mapper)
        {
            _orderService = orderService;
            _mapper = mapper;
        }


        [HttpGet("Get/{id}")]
        public JsonResult GetByOrdinal(int id)
        {
            var order = _orderService.FindByOrdinal(id);
            if (order == null)
            {
                return ErrorResponse("Not found");
            }

            return SingleResult(OrderViewModelHelper.ConvertToViewModel(order));
        }


        [HttpGet("OrdersPaginated")]
        public JsonResult GetAllDataTablePagination([FromQuery]DTAjaxModel param)
        {
            IEnumerable<Order> Orders = _orderService.GetAll();
            var totalCustomers = _orderService.GetAll().Count();
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(param);
            var sortDirection = HttpContext.Request.Query["sSortDir_0"]; // asc or desc
            var sortColumnIndex = Convert.ToInt32(HttpContext.Request.Query["iSortCol_0"]);
            // if (!string.IsNullOrEmpty(param.sSearch)) Orders = Orders.Where(z => z.Description.ToLower().Contains(param.sSearch.ToLower()));

            switch (sortColumnIndex)
            {
                case 1:
                    Orders = sortDirection == "asc" ? Orders.OrderBy(z => z.Description) : Orders.OrderByDescending(z => z.Description);
                    break;
                case 2:
                    Orders = sortDirection == "asc" ? Orders.OrderBy(z => z.OrderDate) : Orders.OrderByDescending(z => z.OrderDate);
                    break;
                case 3:
                    Orders = sortDirection == "asc" ? Orders.OrderBy(z => z.CustomerId) : Orders.OrderByDescending(z => z.CustomerId);
                    break;
                case 4:
                    Orders = sortDirection == "asc" ? Orders.OrderBy(z => z.Status) : Orders.OrderByDescending(z => z.Status);
                    break;
                //case 5:
                //    Orders = sortDirection == "asc" ? Orders.OrderBy(z => z.Phone) : Orders.OrderByDescending(z => z.Phone);
                //    break;
                default:
                    Orders = Orders.OrderBy(z => z.Id);
                    break;


            }
            var filteredOrdersCount = Orders.Count();
            Orders = Orders.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalCustomers,
                iTotalDisplayRecords = filteredOrdersCount,
                aaData = Orders
            });
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var orders = _orderService.GetAll();
            var ordersDTO = _mapper.Map<IList<OrderDto>>(orders);
            return Ok(ordersDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var order = _orderService.GetById(id);
            var orderDTO = _mapper.Map<OrderDto>(order);
            return Ok(orderDTO);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _orderService.Delete(id);
            return Ok();
        }


        [HttpPost("Create")]
        public IActionResult Create([FromBody]OrderDto orderDto)
        {
            var order = _mapper.Map<Order>(orderDto);
            try
            {
                // var ex = orderDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                var obj = _orderService.Create(order,
                                                order.Description,
                                                order.OrderDate.ToString("yyyy-MM-dd HH:mm:ss tt"),
                                                order.CustomerId,
                                                order.Status);
                return Ok(new
                {
                    id = obj.Id, 
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create

        
        [HttpPost("Update")]
        public IActionResult Update([FromBody]OrderDto orderDto)
        {
            var order = _mapper.Map<Order>(orderDto);
            try
            {
                // var ex = orderDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                var cust = _orderService.Update(order,
                                                order.Description,
                                                order.OrderDate.ToString("yyyy-MM-dd HH:mm:ss tt"),
                                                order.CustomerId,
                                                order.Status);
                return Ok(new
                {
                    Id = cust.Id,
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create



    } // End Class
}
