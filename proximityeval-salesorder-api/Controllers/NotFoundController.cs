﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace proximityeval_salesorder_api.Controllers
{
    [Route("/")]
    [Produces("text/plain")]
    public class NotFoundController : BaseController
    {
        
        [HttpGet]
        public string Get()
        {
            return IncorrectUseOfApi();
        }
    } // End of class
}
