﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using proximityeval.salesorder.api.dto.MapperObjects;
using proximityeval.salesorder.api.entities;

namespace proximityeval_salesorder_api.Helpers
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            
            CreateMap<Role, RoleDto>();
            CreateMap<RoleDto, Role>();
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();

            // Eval DTOs
            // Customer
            CreateMap<Customer, CustomerDto>();
            CreateMap<CustomerDto, Customer>();

            // Product
            CreateMap<Product, ProductDto>();
            CreateMap<ProductDto, Product>();

            // Order
            CreateMap<Order, OrderDto>();
            CreateMap<OrderDto, Order>();

            // Order details
            CreateMap<OrderDetail, OrderDetailDto>();
            CreateMap<OrderDetailDto, OrderDetail>();


        }
    }
}
