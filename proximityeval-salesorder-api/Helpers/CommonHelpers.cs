﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

namespace proximityeval_salesorder_api.Helpers
{
    public static class CommonHelpers
    {
        /// Hard code this list for now, as .NET Core 1.0
        /// doesn't have reflection
        public static string IncorrectUsageOfApi()
        {
            var sb = new System.Text.StringBuilder();
            sb.Append($"Incorrect usage of API{Environment.NewLine}");

            return sb.ToString();
        }

        public static string GetVersionNumber()
        {
            return Assembly.GetEntryAssembly()
                .GetCustomAttribute<AssemblyInformationalVersionAttribute>()
                .InformationalVersion;
        }
    }
}
