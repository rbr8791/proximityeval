﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace proximityeval_salesorder_api.Helpers
{
    public static class SecretChecker
    {
        public static bool CheckUserSuppliedSecretValue(string userSuppliedValue, string secretValue)
        {
            return (string.IsNullOrWhiteSpace(userSuppliedValue) || string.IsNullOrWhiteSpace(secretValue)
                    || !string.Equals(userSuppliedValue, secretValue, StringComparison.InvariantCulture));
        }
    }
}
