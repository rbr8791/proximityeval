﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace proximityeval.salesorder.api.entities
{
    public class Product: BaseAuditClass
    {
        public int Id { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public double UnitPrice { get; set; }
        [Required]
        public int UnitsInStock { get; set; }
        [Required]
        public bool Status { get; set; } = true;
    }
}
