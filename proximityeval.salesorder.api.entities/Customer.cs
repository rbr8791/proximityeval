﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace proximityeval.salesorder.api.entities
{
    public class Customer: BaseAuditClass
    {
        public int Id { get; set; }
        [Required]
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public ICollection<Order> Orders { get; set; }
        [Required]
        public bool Status { get; set; } = true;
    }
}
