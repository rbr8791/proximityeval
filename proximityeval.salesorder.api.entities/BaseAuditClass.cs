﻿using System;



    namespace proximityeval.salesorder.api.entities
    {
        public class BaseAuditClass
        {
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

            public DateTime ModifiedAt { get; set; } = DateTime.UtcNow;

            public string createdBy { get; set; } = "";
        }
    }
    

