﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace proximityeval.salesorder.api.entities
{
    public class Order: BaseAuditClass
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime OrderDate { get; set; } = DateTime.UtcNow;
        [Required]
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
        [Required]
        public bool Status { get; set; } = true;
    }
}
