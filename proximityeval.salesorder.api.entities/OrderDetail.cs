﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace proximityeval.salesorder.api.entities
{
    public class OrderDetail: BaseAuditClass
    {
        public int Id { get; set; }
        [Required]
        public Product Product { get; set; }
        [Required]
        [ForeignKey("Order")]
        public int OrderId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public double UnitPrice { get; set; }
        [Required]
        public bool Status { get; set; } = true;

       
    }
}
