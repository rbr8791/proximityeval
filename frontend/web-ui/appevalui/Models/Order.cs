﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace appevalui.Models
{
    public class Order
    {
        public Order() { }
        public Order(int id, string description, DateTime orderDate, int customerId, bool status)
        {
            Id = id;
            Description = description;
            OrderDate = orderDate;
            CustomerId = customerId;
            Status = status;
        }
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime OrderDate { get; set; }
        public int CustomerId { get; set; }
        public bool Status { get; set; }
        
    }
}

