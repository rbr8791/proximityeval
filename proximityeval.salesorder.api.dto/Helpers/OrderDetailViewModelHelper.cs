﻿using System;
using System.Collections.Generic;
using System.Linq;
using proximityeval.salesorder.api.dto.ViewModels;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.dto.MapperObjects;

namespace proximityeval.salesorder.api.dto.Helpers
{
    public static class OrderDetailViewModelHelper
    {
        public static OrderDetailViewModel ConvertToViewModel(OrderDetail dbModel)
        {
            var viewModel = new OrderDetailViewModel
            {
                Id = dbModel.Id,
                Product = dbModel.Product,
                OrderId = dbModel.OrderId,
                Description = dbModel.Description,
                Quantity = dbModel.Quantity,
                UnitPrice = dbModel.UnitPrice,
                Status = dbModel.Status,

            };

            return viewModel;
        }

        public static List<OrderDetailViewModel> ConvertToViewModels(List<OrderDetail> dbModel)
        {
            return dbModel.Select(ConvertToViewModel).ToList();
        }

        public static List<OrderDetailBaseViewModel> ConvertToBaseViewModels(List<OrderDetail> dbModel)
        {
            return dbModel.Select(ConvertToBaseViewModel).ToList();
        }


        private static OrderDetailBaseViewModel ConvertToBaseViewModel(OrderDetail dbModel)
        {
            var viewModel = new OrderDetailBaseViewModel
            {
                Id = dbModel.Id,
                Product = dbModel.Product,
                OrderId = dbModel.OrderId,
                Description = dbModel.Description,
                Quantity = dbModel.Quantity,
                UnitPrice = dbModel.UnitPrice,
                Status = dbModel.Status,
            };
            
            return viewModel;
        }
    } // End Class
}
