﻿using System;
using System.Collections.Generic;
using System.Linq;
using proximityeval.salesorder.api.dto.ViewModels;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.dto.MapperObjects;

namespace proximityeval.salesorder.api.dto.Helpers
{
    public static class OrderViewModelHelper
    {
        public static OrderViewModel ConvertToViewModel(Order dbModel)
        {
            var viewModel = new OrderViewModel
            {
                Id = dbModel.Id,
                Description = dbModel.Description,
                OrderDate = dbModel.OrderDate,
                CustomerId = dbModel.CustomerId,
                OrderDetails = dbModel.OrderDetails,
                Status = dbModel.Status,

            };

            return viewModel;
        }

        public static List<OrderViewModel> ConvertToViewModels(List<Order> dbModel)
        {
            return dbModel.Select(ConvertToViewModel).ToList();
        }

        public static List<OrderBaseViewModel> ConvertToBaseViewModels(List<Order> dbModel)
        {
            return dbModel.Select(ConvertToBaseViewModel).ToList();
        }


        private static OrderBaseViewModel ConvertToBaseViewModel(Order dbModel)
        {
            var viewModel = new OrderBaseViewModel
            {
                Id = dbModel.Id,
                Description = dbModel.Description,
                OrderDate = dbModel.OrderDate,
                CustomerId = dbModel.CustomerId,
                OrderDetails = dbModel.OrderDetails,
                Status = dbModel.Status,
            };


            return viewModel;
        }
    } // End Class
}
