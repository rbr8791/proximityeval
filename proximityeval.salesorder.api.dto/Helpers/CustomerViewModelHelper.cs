﻿using System;
using System.Collections.Generic;
using System.Linq;
using proximityeval.salesorder.api.dto.ViewModels;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.dto.MapperObjects;

namespace proximityeval.salesorder.api.dto.Helpers
{
    public static class CustomerViewModelHelper
    {
        public static CustomerViewModel ConvertToViewModel(Customer dbModel)
        {
            var viewModel = new CustomerViewModel
            {
                Id = dbModel.Id,
                CustomerName = dbModel.CustomerName,
                Address = dbModel.Address,
                City = dbModel.City,
                Region = dbModel.Region,
                PostalCode = dbModel.PostalCode,
                Phone = dbModel.Phone,
                Orders = dbModel.Orders,
                Status = dbModel.Status,
            };

            return viewModel;
        }

        public static List<CustomerViewModel> ConvertToViewModels(List<Customer> dbModel)
        {
            return dbModel.Select(ConvertToViewModel).ToList();
        }

        public static List<CustomerBaseViewModel> ConvertToBaseViewModels(List<Customer> dbModel)
        {
            return dbModel.Select(ConvertToBaseViewModel).ToList();
        }


        private static CustomerBaseViewModel ConvertToBaseViewModel(Customer dbModel)
        {
            var viewModel = new CustomerBaseViewModel
            {
                Id = dbModel.Id,
                CustomerName = dbModel.CustomerName,
                Address = dbModel.Address,
                City = dbModel.City,
                Region = dbModel.Region,
                PostalCode = dbModel.PostalCode,
                Phone = dbModel.Phone,
                Orders = dbModel.Orders,
                Status = dbModel.Status,
            };


            return viewModel;
        }
    } // End Class
}
