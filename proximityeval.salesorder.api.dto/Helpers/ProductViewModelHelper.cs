﻿using System;
using System.Collections.Generic;
using System.Linq;
using proximityeval.salesorder.api.dto.ViewModels;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.dto.MapperObjects;

namespace proximityeval.salesorder.api.dto.Helpers
{
    public static class ProductViewModelHelper
    {
        public static ProductViewModel ConvertToViewModel(Product dbModel)
        {
            var viewModel = new ProductViewModel
            {
                Id = dbModel.Id,
                ProductName = dbModel.ProductName,
                UnitPrice = dbModel.UnitPrice,
                UnitsInStock = dbModel.UnitsInStock,
                Status = dbModel.Status,
            };

            return viewModel;
        }

        public static List<ProductViewModel> ConvertToViewModels(List<Product> dbModel)
        {
            return dbModel.Select(ConvertToViewModel).ToList();
        }

        public static List<ProductBaseViewModel> ConvertToBaseViewModels(List<Product> dbModel)
        {
            return dbModel.Select(ConvertToBaseViewModel).ToList();
        }


        private static ProductBaseViewModel ConvertToBaseViewModel(Product dbModel)
        {
            var viewModel = new ProductBaseViewModel
            {
                Id = dbModel.Id,
                ProductName = dbModel.ProductName,
                UnitPrice = dbModel.UnitPrice,
                UnitsInStock = dbModel.UnitsInStock,
                Status = dbModel.Status,
            };


            return viewModel;
        }
    } // End Class
}
