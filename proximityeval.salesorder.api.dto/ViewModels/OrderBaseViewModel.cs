﻿using proximityeval.salesorder.api.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace proximityeval.salesorder.api.dto.ViewModels
{
    public class OrderBaseViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime OrderDate { get; set; } = DateTime.UtcNow;
        public int CustomerId { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
        public bool Status { get; set; } = true;
    }
}
