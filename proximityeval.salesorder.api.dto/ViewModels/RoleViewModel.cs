﻿using System;
using System.Collections.Generic;
using System.Text;

namespace proximityeval.salesorder.api.dto.ViewModels
{
    public class RoleViewModel : BaseViewModel
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public bool Status { get; set; }
    }
}
