﻿using System;
using System.Collections.Generic;
using System.Text;
using proximityeval.salesorder.api.dto.MapperObjects;

namespace proximityeval.salesorder.api.dto.ViewModels
{
    public class UserBaseViewModel: BaseViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }

        public RoleDto Role { get; set; }

        public bool Enabled { get; set; }
    }
}
