﻿using proximityeval.salesorder.api.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace proximityeval.salesorder.api.dto.ViewModels
{
    public class CustomerBaseViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public ICollection<Order> Orders { get; set; }
        public bool Status { get; set; } = true;
    }
}
