﻿using System;
using System.Collections.Generic;
using System.Text;
using proximityeval.salesorder.api.entities;

namespace proximityeval.salesorder.api.dto.ViewModels
{
    public class OrderDetailViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public Product Product { get; set; }
        public int OrderId { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public bool Status { get; set; } = true;
    }
}
