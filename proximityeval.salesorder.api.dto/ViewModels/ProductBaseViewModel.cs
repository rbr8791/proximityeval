﻿using proximityeval.salesorder.api.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace proximityeval.salesorder.api.dto.ViewModels
{
    public class ProductBaseViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public double UnitPrice { get; set; }
        public int UnitsInStock { get; set; }
        public bool Status { get; set; } = true;
    }
}
