﻿using System;
using System.Collections.Generic;
using System.Text;
using proximityeval.salesorder.api.dto.Helpers.Infrastructure;
using proximityeval.salesorder.api.entities;
using System.Linq.Expressions;
using System.Linq;

namespace proximityeval.salesorder.api.dto.MapperObjects
{
    public class OrderDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime OrderDate { get; set; } = DateTime.UtcNow;
        public int CustomerId { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
        public bool Status { get; set; } = true;

        public static OrderDto FromModel(Order model)
        {
            return new OrderDto()
            {
                Id = model.Id,
                Description = model.Description,
                OrderDate = model.OrderDate,
                CustomerId = model.CustomerId,
                OrderDetails = model.OrderDetails,
                Status = model.Status,
                
            };
        }

        public Order ToModel()
        {
            return new Order()
            {
                Id = Id,
                Description = Description,
                OrderDate = OrderDate,
                CustomerId = CustomerId,
                OrderDetails = OrderDetails,
                Status = Status,
            };
        }
    }
}
