﻿using System;
using System.Collections.Generic;
using System.Text;
using proximityeval.salesorder.api.dto.Helpers.Infrastructure;
using proximityeval.salesorder.api.entities;
using System.Linq.Expressions;
using System.Linq;

namespace proximityeval.salesorder.api.dto.MapperObjects
{
    public class OrderDetailDto
    {
        public int Id { get; set; }
        public Product Product { get; set; }
        public int OrderId { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public bool Status { get; set; } = true;

        public static OrderDetailDto FromModel(OrderDetail model)
        {
            return new OrderDetailDto()
            {
                Id = model.Id,
                Product = model.Product,
                OrderId = model.OrderId,
                Description = model.Description,
                Quantity = model.Quantity,
                UnitPrice = model.UnitPrice,
                Status = model.Status,
                
            };
        }

        public OrderDetail ToModel()
        {
            return new OrderDetail()
            {
                Id = Id,
                Product = Product,
                OrderId = OrderId,
                Description = Description,
                Quantity = Quantity,
                UnitPrice = UnitPrice,
                Status = Status,
            };
        }
    }
}
