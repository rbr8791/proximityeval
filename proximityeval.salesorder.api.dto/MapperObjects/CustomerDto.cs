﻿using System;
using System.Collections.Generic;
using System.Text;
using proximityeval.salesorder.api.dto.Helpers.Infrastructure;
using proximityeval.salesorder.api.entities;
using System.Linq.Expressions;
using System.Linq;

namespace proximityeval.salesorder.api.dto.MapperObjects
{
    public class CustomerDto
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public ICollection<Order> Orders { get; set; }
        public bool Status { get; set; } = true;

        public static CustomerDto FromModel(Customer model)
        {
            return new CustomerDto()
            {
                Id = model.Id,
                CustomerName = model.CustomerName,
                Address = model.Address,
                City = model.City,
                Region = model.Region,
                PostalCode = model.PostalCode,
                Phone = model.Phone,
                Orders = model.Orders,
                Status = model.Status,
            };
        }

        public Customer ToModel()
        {
            return new Customer()
            {
                Id = Id,
                CustomerName = CustomerName,
                Address = Address,
                City = City,
                Region = Region,
                PostalCode = PostalCode,
                Phone = Phone,
                Orders = Orders,
                Status = Status,
            };
        }
    }
}
