﻿using System;
using System.Collections.Generic;
using System.Text;
using proximityeval.salesorder.api.dto.Helpers.Infrastructure;
using proximityeval.salesorder.api.entities;
using System.Linq.Expressions;
using System.Linq;

namespace proximityeval.salesorder.api.dto.MapperObjects
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public double UnitPrice { get; set; }
        public int UnitsInStock { get; set; }
        public bool Status { get; set; } = true;

        public static ProductDto FromModel(Product model)
        {
            return new ProductDto()
            {
                Id = model.Id,
                ProductName = model.ProductName,
                UnitPrice = model.UnitPrice,
                UnitsInStock = model.UnitsInStock,
                Status = model.Status,
                
            };
        }

        public Product ToModel()
        {
            return new Product()
            {
                Id = Id,
                ProductName = ProductName,
                UnitPrice = UnitPrice,
                UnitsInStock = UnitsInStock,
                Status = Status,
            };
        }
    }
}
