﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using proximityeval.salesorder.api.entities;


namespace proximityeval.salesorder.api.persistence.Helpers
{
    public class DatabaseSeeder
    {
        private readonly IProximityEvalContext _context;

        public DatabaseSeeder(ProximityEvalContext context)
        {
            _context = context;
        }

        public async Task<int> SeedRolesFromJson(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentException($"Value of {filePath} must be supplied to {nameof(SeedRolesFromJson)}");
            }
            if (!File.Exists(filePath))
            {
                throw new ArgumentException($"The file { filePath} does not exist");
            }
            var dataSet = File.ReadAllText(filePath);
            var seedData = JsonConvert.DeserializeObject<List<Role>>(dataSet);

            // ensure that we only get the distinct books (based on their name)
            var distinctSeedData = seedData.GroupBy(b => b.RoleName).Select(b => b.First());

            _context.Roles.AddRange(distinctSeedData);


            return await _context.SaveChangesAsync();
        }

    }
}
