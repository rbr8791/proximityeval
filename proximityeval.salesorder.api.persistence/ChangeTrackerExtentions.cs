﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using proximityeval.salesorder.api.entities;

namespace proximityeval.salesorder.api.persistence
{
    public static class ChangeTrackerExtentions
    {
        public static void ApplyAuditInformation(this ChangeTracker changeTracker)
        {
            foreach (var entry in changeTracker.Entries())
            {
                if (!(entry.Entity is BaseAuditClass baseAudit)) continue;

                var now = DateTime.UtcNow;
                switch (entry.State)
                {
                    case EntityState.Modified:
                        baseAudit.CreatedAt = now;
                        baseAudit.ModifiedAt = now;
                        break;

                    case EntityState.Added:
                        baseAudit.CreatedAt = now;
                        break;
                }
            }
        }
    }
}
