﻿using System.IO;
using System.Linq;
using proximityeval.salesorder.api.persistence.Helpers;
using proximityeval.salesorder.api.persistence;
using proximityeval.salesorder.api.entities;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;

namespace proximityeval.salesorder.api.persistence
{
    public static class ProximityEvalContextExtensions
    {
        public static int EnsureSeedData(this ProximityEvalContext context)
        {
            var rolesCount = default(int);
          
            // DB Seeders
            var dbSeeder = new DatabaseSeeder(context);

            // Products Seeder
            var prods = context.Products;
            int total_prods = 0;
            total_prods = prods.Count();
            if (total_prods <= 0)
            {
                string prd_name = "Product #: ";
                string prd_name_s = "";
                int i = 1;
                for (int a=1; a<=1500; a++)
                {
                    prd_name_s = "";
                    prd_name_s = prd_name + " " + i.ToString();
                    var p = new Product();
                    p.ProductName = prd_name_s;
                    p.UnitPrice = 5.00;
                    p.UnitsInStock = 150;
                    p.Status = false;
                    
                    

                    context.Products.Add(p);
                    context.SaveChanges();
                    i += 1;

                }
            }

            // Customers Seeder
            var custs = context.Customers;
            int total_custs = 0;
            total_custs = custs.Count();
            if (total_custs <= 0)
            {
                string prd_name = "Customer #: ";
                string prd_name_s = "";
                int i = 1;
                for (int a = 1; a <= 1500; a++)
                {
                    prd_name_s = "";
                    prd_name_s = prd_name + " " + i.ToString();
                    var p = new Customer();
                    p.CustomerName = prd_name_s;
                    p.Address = "Address # " + i.ToString();
                    p.City = "City # " + i.ToString();
                    p.Region = "Region # " + i.ToString();
                    p.PostalCode = "PostalCode # " + i.ToString();
                    p.Phone = "Phone # " + i.ToString();

                    p.Status = true;



                    context.Customers.Add(p);
                    context.SaveChanges();
                    i += 1;

                }
            }

            // Orders Seeder
            var ords = context.Orders;
            int total_ords = 0;
            
            total_ords = ords.Count();
            if (total_ords <= 0)
            {
                string ord_name = "Order #: ";
                string ord_name_s = "";
                int i = 1;
                for (int a = 1; a <= 150; a++)
                {
                    ord_name_s = "";
                    ord_name_s = ord_name + " " + i.ToString();
                    var o = new Order();

                    
                    Product p1 = context.Products.Find(1);
                    Product p2 = context.Products.Find(2);
                    Product p3 = context.Products.Find(3);
                    Product p4 = context.Products.Find(4);
                    Product p5 = context.Products.Find(5);
                    Product p6 = context.Products.Find(6);
                    Customer c1 = context.Customers.Find(1);
                    ICollection<Product> products = new Collection<Product>();
                    ICollection<OrderDetail> o_details = new Collection<OrderDetail>();
                    products.Add(p1);
                    products.Add(p2);
                    products.Add(p3);
                    products.Add(p4);
                    products.Add(p5);
                    products.Add(p6);
                    o.OrderDate = DateTime.Now;
                    o.Description = "Seeded test Order #: " + i.ToString();
                    o.CustomerId = c1.Id;
                    context.Orders.Add(o);
                    context.SaveChanges();
                    int current_order_id = o.Id;

                    for (int x=1; x<=6; x++)
                    {
                        var det = new OrderDetail();
                        switch(x)
                        {
                            case 1:
                                det.Product = p1;
                                det.Quantity = 1;
                                det.UnitPrice = 1.25;
                                break;
                            case 2:
                                det.Product = p2;
                                det.Quantity = 2;
                                det.UnitPrice = 5.35;
                                break;
                            case 3:
                                det.Product = p3;
                                det.Quantity = 3;
                                det.UnitPrice = 10.25;
                                break;
                            case 4:
                                det.Product = p4;
                                det.Quantity = 4;
                                det.UnitPrice = 5.45;
                                break;
                            case 5:
                                det.Product = p5;
                                det.Quantity = 5;
                                det.UnitPrice = 3.25;
                                break;
                            case 6:
                                det.Product = p6;
                                det.Quantity = 6;
                                det.UnitPrice = 6.50;
                                break;
                        }
                        det.OrderId = current_order_id;
                        det.Description = "Description test# " + i.ToString();
                        det.Status = true;
                        context.OrderDetails.Add(det);
                        context.SaveChanges();
                        o_details.Add(det);

                    }

                    
                    i += 1;

                }
            }

            if (!context.Roles.Any())
            {
                var pathToSeedData = Path.Combine(Directory.GetCurrentDirectory(), "SeedData", "RoleSeedData.json");
                rolesCount = dbSeeder.SeedRolesFromJson(pathToSeedData).Result;
                if (rolesCount > 0)
                {


                    int adminId = context
                      .Roles
                      .Where(r => r.RoleName == "Admin")
                      .Select(r => r.Id)
                      .SingleOrDefault();

                    Role role = context.Roles.Find(adminId);

                    if (adminId > 0 && role != null)
                    {
                        byte[] passwordHash, passwordSalt;
                        var password = "admin";
                        Tools.CreatePasswordHash(password, out passwordHash, out passwordSalt);

                        // Add user Admin
                        var user = new User();
                        user.FirstName = "admin";
                        user.LastName = "admin";
                        user.Username = "admin";
                        user.Password = "";
                        user.PasswordHash = passwordHash;
                        user.PasswordSalt = passwordSalt;
                        user.Role = role;
                        user.Enabled = true;

                        context.Users.Add(user);
                        context.SaveChanges();
                    }

                    int operatorId = context
                      .Roles
                      .Where(r => r.RoleName == "Operator")
                      .Select(r => r.Id)
                      .SingleOrDefault();

                    Role roleOperator = context.Roles.Find(operatorId);

                    
                    if (operatorId > 0 && roleOperator != null)
                    {
                        byte[] passwordHash, passwordSalt;
                        var password = "operator";
                        Tools.CreatePasswordHash(password, out passwordHash, out passwordSalt);

                        // Add user Admin
                        var userOperator = new User();
                        userOperator.FirstName = "operator";
                        userOperator.LastName = "operator";
                        userOperator.Username = "operator";
                        userOperator.Password = "";
                        userOperator.PasswordHash = passwordHash;
                        userOperator.PasswordSalt = passwordSalt;
                        userOperator.Role = roleOperator;
                        userOperator.Enabled = true;

                        context.Users.Add(userOperator);
                        context.SaveChanges();
                    }


                    if (operatorId > 0 && roleOperator != null)
                    {
                        byte[] passwordHash, passwordSalt;
                        var password = "operator";
                        Tools.CreatePasswordHash(password, out passwordHash, out passwordSalt);

                        // Add user Admin
                        var userOperator = new User();
                        userOperator.FirstName = "operator";
                        userOperator.LastName = "operator";
                        userOperator.Username = "operator";
                        userOperator.Password = "";
                        userOperator.PasswordHash = passwordHash;
                        userOperator.PasswordSalt = passwordSalt;
                        userOperator.Role = roleOperator;
                        userOperator.Enabled = true;

                        context.Users.Add(userOperator);
                        context.SaveChanges();
                    }

                    
                }
            }



            return rolesCount;
        }
    }
}
