﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using proximityeval.salesorder.api.entities;


namespace proximityeval.salesorder.api.persistence
{
    public class ProximityEvalContext: DbContext, IProximityEvalContext
    {
        public ProximityEvalContext(DbContextOptions<ProximityEvalContext> options) : base(options) { }
        public ProximityEvalContext() { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddPrimaryKeys();
            
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            ChangeTracker.ApplyAuditInformation();

            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }


        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }


        // Eval entities
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

    }
}
