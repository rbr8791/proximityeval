﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using proximityeval.salesorder.api.common;

namespace proximityeval.salesorder.api.persistence
{
    public class ProximityEvalContextFactory : IDesignTimeDbContextFactory<ProximityEvalContext>
    {
        private static string DbConnectionString => new DatabaseConfiguration().GetDatabaseConnectionString();

        public ProximityEvalContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProximityEvalContext>();
            // Change here for use another Database Dialect connection string - R@ul Berrios

            optionsBuilder.UseSqlServer(DbConnectionString);

            // Change here for use another Database Dialect connection string - R@ul Berrios

            return new ProximityEvalContext(optionsBuilder.Options);
        }
    }
}
