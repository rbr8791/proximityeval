﻿@echo off

REM Execute database migrations
REM Author: R@ul Berrios Rivera

echo "Install dotnet ef version requirement"
dotnet tool install --global dotnet-ef --version 3.0.0

echo "Creating initial migration"
dotnet ef migrations add InitialMigration

echo "Running database updates"
dotnet ef database update

REM only if you are using SQlite database for testing purposes
REM echo "Copying sqlite database file to running directory, this will overwrite any existing db file"
REM xcopy /y proximityevalSalesOrderAPI.db ..\proximityeval-salesorder-api\proximityevalSalesOrderAPI.db

echo "We will start the database seeding process..."

echo "cd ..\proximityeval-salesorder-api"
cd ..\proximityeval-salesorder-api
echo "Building project..."
dotnet build

echo "Running project for fist time and seeding the database"

dotnet run



