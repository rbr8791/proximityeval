﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Generic;
using proximityeval.salesorder.api.entities;

namespace proximityeval.salesorder.api.dal.Interfaces
{
    public interface IProductService
    {
        IEnumerable<Product> GetAll();
        Product GetById(int id);
        Product Create(Product product, 
            string productName,
            double unitPrice,
            int unitsInStock,
            bool status);
        Product Update(Product product,
            string productName,
            double unitPrice,
            int unitsInStock,
            bool status);
        void Delete(int id);
        Product FindByOrdinal(int id);

    }
}
