﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Generic;
using proximityeval.salesorder.api.entities;

namespace proximityeval.salesorder.api.dal.Interfaces
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetAll();
        Customer GetById(int id);
        Customer Create(Customer customer, string customerName, string address, string city, string region, string postalCode, string phone, bool status);
        Customer Update(Customer customer, string customerName, string address, string city, string region, string postalCode, string phone, bool status);
        void Delete(int id);
        Customer FindByOrdinal(int id);
    }
}
