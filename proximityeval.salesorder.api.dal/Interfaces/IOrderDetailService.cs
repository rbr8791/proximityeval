﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Generic;
using proximityeval.salesorder.api.entities;

namespace proximityeval.salesorder.api.dal.Interfaces
{
    public interface IOrderDetailService
    {
        IEnumerable<OrderDetail> GetAll();
        OrderDetail GetById(int id);
        OrderDetail Create(OrderDetail orderDetail, 
            int productId,
            int orderId,
            string description,
            int quantity,
            double unitPrice,
            bool status);
        OrderDetail Update(OrderDetail orderDetail,
            int productId,
            int orderId,
            string description,
            int quantity,
            double unitPrice,
            bool status);
        void Delete(int id);
        OrderDetail FindByOrdinal(int id);



    }
}
