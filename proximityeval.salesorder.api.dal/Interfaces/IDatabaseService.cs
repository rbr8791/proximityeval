﻿using System;
using System.Collections.Generic;
using System.Text;
using proximityeval.salesorder.api.entities;
using System.Threading.Tasks;

namespace proximityeval.salesorder.api.dal.Interfaces
{
    public interface IDatabaseService
    {
        bool ClearDatabase();

        int SeedDatabase();
               
        Task<int> SaveAnyChanges();
    } // End Interface
}
