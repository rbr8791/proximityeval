﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Generic;
using proximityeval.salesorder.api.entities;

namespace proximityeval.salesorder.api.dal.Interfaces
{
    public interface IRoleService
    {
        IEnumerable<Role> GetAll();
        Role GetById(int id);
        Role Create(Role role, string roleName);
        Role Update(Role role, string roleName = null);
        void Delete(int id);
        Role FindByOrdinal(int id);
    }
}
