﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Generic;
using proximityeval.salesorder.api.entities;

namespace proximityeval.salesorder.api.dal.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<Order> GetAll();
        Order GetById(int id);
        Order Create(Order customer, 
            string description,
            string orderDate,
            int customerId,
            bool status);
        Order Update(Order customer,
            string description,
            string orderDate,
            int customerId,
            bool status);
        void Delete(int id);
        Order FindByOrdinal(int id);

      
    }
}
