﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.persistence;
using Microsoft.EntityFrameworkCore;
using proximityeval.salesorder.api.dal.Interfaces;
using System;

namespace proximityeval.salesorder.api.dal
{
    public class RoleService : IRoleService
    {
        //private ApplDbContext _dwContext;


        private ProximityEvalContext _context;

        public RoleService(ProximityEvalContext context)
        {
            _context = context;
        } // End DataContext

        public Role FindByOrdinal(int id)
        {
            return BaseQuery()
                .FirstOrDefault(role => role.Id == id);
        }

        private IEnumerable<Role> BaseQuery()
        {
            
            return _context.Roles;
            
        }
        public IEnumerable<Role> GetAll()
        {
            return BaseQuery();
            //.FirstOrDefault(role => role.Id == id);
        } // End IEnumerable

        public Role GetById(int id)
        {
            return BaseQuery()
                .FirstOrDefault(role => role.Id == id);
        } // End GetById

        public Role Create(Role role, string roleName)
        {
            // Validation
            if (string.IsNullOrWhiteSpace(roleName))
                throw new System.ApplicationException("The role name can not be null or empty");

            role.RoleName = roleName;

            _context.Roles.Add(role);
            _context.SaveChanges();

            return role;

        } // End Create


        public Role Update(Role roleParam, string roleName = null)
        {
            var role = _context.Roles.Find(roleParam.Id);
            if (role == null)
                throw new System.ApplicationException("Role not found");

            if (roleParam.Id != role.Id)
            {
                // username has changed so check if the new username is already taken
                if (_context.Roles.Any(x => x.RoleName == roleParam.RoleName))
                    throw new System.ApplicationException("Role Name " + roleParam.RoleName + " is already taken");
            }


            role.RoleName = roleParam.RoleName;


            _context.Roles.Update(role);
            _context.SaveChanges();
            return role;
        } // End Update


        public void Delete(int id)
        {
            var role = _context.Roles.Find(id);
            if (role != null)
            {
                _context.Roles.Remove(role);
                _context.SaveChanges();
            }
        } // End Delete
    }
}
