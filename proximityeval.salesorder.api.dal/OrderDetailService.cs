﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.persistence;
using Microsoft.EntityFrameworkCore;
using proximityeval.salesorder.api.dal.Interfaces;
using System;

namespace proximityeval.salesorder.api.dal
{
    public class OrderDetailService : IOrderDetailService
    {
       
        private ProximityEvalContext _context;

        public OrderDetailService(ProximityEvalContext context)
        {
            _context = context;
        } // End DataContext

        public OrderDetail FindByOrdinal(int id)
        {
            return BaseQuery()
                .FirstOrDefault(orderDetail => orderDetail.Id == id);
        }

        private IEnumerable<OrderDetail> BaseQuery()
        {

            return _context.OrderDetails;

        }
        public IEnumerable<OrderDetail> GetAll()
        {
            return BaseQuery();
            
        } // End IEnumerable

        public OrderDetail GetById(int id)
        {
            return BaseQuery()
                .FirstOrDefault(orderDetail => orderDetail.Id == id);
        } // End GetById

        public OrderDetail Create(OrderDetail orderDetail,
            int productId,
            int orderId,
            string description,
            int quantity,
            double unitPrice,
            bool status)
        {
            // Validation
            if (string.IsNullOrWhiteSpace(description))
                throw new System.ApplicationException("The orderDetail description can not be null or empty");

            Product produc = _context.Products.FirstOrDefault(product => product.Id == productId);
            Order orde = _context.Orders.FirstOrDefault(order => order.Id == orderId);
            orderDetail.Product = produc;
            orderDetail.OrderId = orderId;
            orderDetail.Description = description;
            orderDetail.Quantity = quantity;
            orderDetail.UnitPrice = unitPrice;
            orderDetail.Status = status;
          

            _context.OrderDetails.Add(orderDetail);
            _context.SaveChanges();

            return orderDetail;

        } // End Create


        public OrderDetail Update(OrderDetail orderDetail,
            int productId,
            int orderId,
            string description,
            int quantity,
            double unitPrice,
            bool status)
        {
            var j = _context.OrderDetails.Find(orderDetail.Id);
            if (j == null)
                throw new System.ApplicationException("OrderDetail not found");

            
            Product produc = _context.Products.FirstOrDefault(product => product.Id == productId);
            Order orde = _context.Orders.FirstOrDefault(order => order.Id == orderId);
            j.Product = produc;
            j.OrderId = orderId;
            j.Description = description;
            j.Quantity = quantity;
            j.UnitPrice = unitPrice;
            j.Status = status;


            _context.OrderDetails.Update(j);
            _context.SaveChanges();
            return j;
        } // End Update


        public void Delete(int id)
        {
            var orderDetail = _context.OrderDetails.Find(id);
            if (orderDetail != null)
            {
                _context.OrderDetails.Remove(orderDetail);
                _context.SaveChanges();
            }
        } // End Delete
    }
}
