﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.persistence;
using Microsoft.EntityFrameworkCore;
using proximityeval.salesorder.api.dal.Interfaces;
using System;

namespace proximityeval.salesorder.api.dal
{
    public class OrderService : IOrderService
    {
       
        private ProximityEvalContext _context;

        public OrderService(ProximityEvalContext context)
        {
            _context = context;
        } // End DataContext

        public Order FindByOrdinal(int id)
        {
            return BaseQuery()
                .FirstOrDefault(order => order.Id == id);
        }

        private IEnumerable<Order> BaseQuery()
        {

            return _context.Orders;

        }
        public IEnumerable<Order> GetAll()
        {
            return BaseQuery();
            
        } // End IEnumerable

        public Order GetById(int id)
        {
            return BaseQuery()
                .FirstOrDefault(order => order.Id == id);
        } // End GetById

        public Order Create(Order order,
            string description,
            string orderDate,
            int customerId,
            bool status)
        {
            // Validation
            if (string.IsNullOrWhiteSpace(description))
                throw new System.ApplicationException("The order description can not be null or empty");

            order.Description = description;
            order.OrderDate = DateTime.Parse(orderDate);
            order.CustomerId = customerId;
            order.Status = status;
          

            _context.Orders.Add(order);
            _context.SaveChanges();

            return order;

        } // End Create


        public Order Update(Order order,
            string description,
            string orderDate,
            int customerId,
            bool status)
        {
            var j = _context.Orders.Find(order.Id);
            if (j == null)
                throw new System.ApplicationException("Order not found");

            


            j.Description = description;
            j.OrderDate = DateTime.Parse(orderDate);
            j.CustomerId = customerId;
            j.Status = status;
            


            _context.Orders.Update(j);
            _context.SaveChanges();
            return j;
        } // End Update


        public void Delete(int id)
        {
            var order = _context.Orders.Find(id);
            if (order != null)
            {
                _context.Orders.Remove(order);
                _context.SaveChanges();
            }
        } // End Delete
    }
}
