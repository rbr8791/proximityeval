﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.persistence;
using Microsoft.EntityFrameworkCore;
using proximityeval.salesorder.api.dal.Interfaces;
using System;

namespace proximityeval.salesorder.api.dal
{
    public class CustomerService : ICustomerService
    {
       
        private ProximityEvalContext _context;

        public CustomerService(ProximityEvalContext context)
        {
            _context = context;
        } // End DataContext

        public Customer FindByOrdinal(int id)
        {
            return BaseQuery()
                .FirstOrDefault(customer => customer.Id == id);
        }

        private IEnumerable<Customer> BaseQuery()
        {

            return _context.Customers;

        }
        public IEnumerable<Customer> GetAll()
        {
            return BaseQuery();
            
        } // End IEnumerable

        public Customer GetById(int id)
        {
            return BaseQuery()
                .FirstOrDefault(customer => customer.Id == id);
        } // End GetById

        public Customer Create(Customer customer, string customerName, string address, string city, string region, string postalCode, string phone, bool status)
        {
            // Validation
            if (string.IsNullOrWhiteSpace(customerName))
                throw new System.ApplicationException("The customer name can not be null or empty");

            customer.CustomerName = customerName;
            customer.Address = address;
            customer.City = city;
            customer.Region = region;
            customer.PostalCode = postalCode;
            customer.Phone = phone;
            customer.Status = status;
          

            _context.Customers.Add(customer);
            _context.SaveChanges();

            return customer;

        } // End Create


        public Customer Update(Customer customer, string customerName, string address, string city, string region, string postalCode, string phone, bool status)
        {
            var j = _context.Customers.Find(customer.Id);
            if (j == null)
                throw new System.ApplicationException("Customer not found");

            


            j.CustomerName = customerName;
            j.Address = address;
            j.City = city;
            j.Region = region;
            j.PostalCode = postalCode;
            j.Phone = phone;
            j.Status = status;


            _context.Customers.Update(j);
            _context.SaveChanges();
            return j;
        } // End Update


        public void Delete(int id)
        {
            var customer = _context.Customers.Find(id);
            if (customer != null)
            {
                _context.Customers.Remove(customer);
                _context.SaveChanges();
            }
        } // End Delete
    }
}
