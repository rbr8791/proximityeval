﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using proximityeval.salesorder.api.entities;
using proximityeval.salesorder.api.persistence;
using Microsoft.EntityFrameworkCore;
using proximityeval.salesorder.api.dal.Interfaces;
using System;

namespace proximityeval.salesorder.api.dal
{
    public class ProductService : IProductService
    {
       
        private ProximityEvalContext _context;

        public ProductService(ProximityEvalContext context)
        {
            _context = context;
        } // End DataContext

        public Product FindByOrdinal(int id)
        {
            return BaseQuery()
                .FirstOrDefault(product => product.Id == id);
        }

        private IEnumerable<Product> BaseQuery()
        {

            return _context.Products;

        }
        public IEnumerable<Product> GetAll()
        {
            return BaseQuery();
            
        } // End IEnumerable

        public Product GetById(int id)
        {
            return BaseQuery()
                .FirstOrDefault(product => product.Id == id);
        } // End GetById

        public Product Create(Product product,
            string productName,
            double unitPrice,
            int unitsInStock,
            bool status)
        {
            // Validation
            if (string.IsNullOrWhiteSpace(productName))
                throw new System.ApplicationException("The product name can not be null or empty");

            product.ProductName = productName;
            product.UnitPrice = unitPrice;
            product.UnitsInStock = unitsInStock;
            product.Status = status;
          

            _context.Products.Add(product);
            _context.SaveChanges();

            return product;

        } // End Create


        public Product Update(Product product,
            string productName,
            double unitPrice,
            int unitsInStock,
            bool status)
        {
            var j = _context.Products.Find(product.Id);
            if (j == null)
                throw new System.ApplicationException("Product not found");

            


            j.ProductName = productName;
            j.UnitPrice = unitPrice;
            j.UnitsInStock = unitsInStock;
            j.Status = status;
            


            _context.Products.Update(j);
            _context.SaveChanges();
            return j;
        } // End Update


        public void Delete(int id)
        {
            var product = _context.Products.Find(id);
            if (product != null)
            {
                _context.Products.Remove(product);
                _context.SaveChanges();
            }
        } // End Delete
    }
}
