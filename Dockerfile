FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 5000
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY ["proximityeval-salesorder-api/proximityeval-salesorder-api.csproj", "proximityeval-salesorder-api/"]
COPY ["proximityeval.salesorder.api.common/proximityeval.salesorder.api.common.csproj", "proximityeval.salesorder.api.common/"]
COPY ["proximityeval.salesorder.api.dto/proximityeval.salesorder.api.dto.csproj", "proximityeval.salesorder.api.dto/"]
COPY ["proximityeval.salesorder.api.entities/proximityeval.salesorder.api.entities.csproj", "proximityeval.salesorder.api.entities/"]
COPY ["proximityeval.salesorder.api.dal/proximityeval.salesorder.api.dal.csproj", "proximityeval.salesorder.api.dal/"]
COPY ["proximityeval.salesorder.api.persistence/proximityeval.salesorder.api.persistence.csproj", "proximityeval.salesorder.api.persistence/"]
RUN dotnet restore "proximityeval-salesorder-api/proximityeval-salesorder-api.csproj"
COPY . .
WORKDIR "/src/proximityeval-salesorder-api"
RUN dotnet build "proximityeval-salesorder-api.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "proximityeval-salesorder-api.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "proximityeval-salesorder-api.dll"]